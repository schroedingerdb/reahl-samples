# reahl-samples

More details here: http://schroedingerdb.de/client/reahl

## sample project setup
```
# create virtual python env
virtualenv -p python3 venv
source venv/bin/activate 
pip install -r requirements.txt

# setup reahl sample project
cd <sample_project>
reahl setup -- develop -N
reahl createdbuser etc
reahl createdb etc
reahl createdbtables etc
reahl serve etc
```