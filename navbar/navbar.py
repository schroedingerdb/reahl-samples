
from __future__ import print_function, unicode_literals, absolute_import, division
from reahl.web.fw import UserInterface
from reahl.web.bootstrap.ui import HTML5Page
from reahl.web.bootstrap.navbar import Navbar, ResponsiveLayout

class MyTestUI(UserInterface):
    def assemble(self):
        self.define_view('/',       title='Home', page=MyTestPage.factory())        

class MyTestPage(HTML5Page):
    def __init__(self, view):
        super(MyTestPage, self).__init__(view)

        layout = ResponsiveLayout('md', colour_theme='light', bg_scheme='light')        
        navbar = Navbar(view, css_id='sp_nav').use_layout(layout)
        navbar.layout.set_brand_text('MyTestPage')

        self.body.add_child(navbar)